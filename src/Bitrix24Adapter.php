<?php

namespace Nashimoari;

use Bitrix24\Bitrix24;
use Bitrix24\Exceptions\Bitrix24Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Bitrix24Adapter
{
    private $code;
    private $isInit = false;
    private $securitySettings;


    /**
     * @var Bitrix24
     */
    private $bitrix24;


    public function __construct($code)
    {
        $this->code = $code;

        /**
         * Пробуем загрузить настройки безопасности для указанного кода
         */
        $this->loadSecuritySettings();
    }

    /**
     * @return Bitrix24
     * @throws Bitrix24Exception
     */
    public function createBitrixApp()
    {

        $application_id = $this->securitySettings['application_id'];
        $application_secret = $this->securitySettings['application_secret'];
        $application_scope = $this->securitySettings['application_scope'];
        $application_redirect = $this->securitySettings['application_redirect'];
        $domain = $this->securitySettings['domain'];
        $memberId = $this->securitySettings['member_id'];


        $this->bitrix24 = new Bitrix24(false);

        $this->bitrix24->setApplicationId($application_id);
        $this->bitrix24->setApplicationSecret($application_secret);
        $this->bitrix24->setApplicationScope(explode(",", $application_scope));

        if (strlen($application_redirect) > 0) {
            $this->bitrix24->setRedirectUri($application_redirect);
        };

        $this->bitrix24->setRetriesToConnectCount(3);
        $this->bitrix24->setRetriesToConnectTimeout(3000000);

        $this->bitrix24->setDomain($domain);
        $this->bitrix24->setMemberId($memberId);


        $this->applyAuthData();

        $this->checkTokenExpire();


        $this->bitrix24->setDomain($domain);
        return $this->bitrix24;
    }

    private function checkTokenExpire()
    {
        $currTimeStamp = time();
        if ($currTimeStamp < ($this->securitySettings['expires'] - 1)) {
            Log::debug('seconds to token expired:'.($this->securitySettings['expires'] - $currTimeStamp));
            return;
        }

        Log::debug('Token expired');
        $accessData = $this->bitrix24->getNewAccessToken();
        Log::debug(print_r($accessData, 1));
        $this->updateAuthData($accessData);
        $this->applyAuthData();
    }

    /**
     * Обновляем аутентификационные данные
     */
    private function applyAuthData()
    {
        $this->bitrix24->setAccessToken($this->securitySettings['access_token']);
        $this->bitrix24->setRefreshToken($this->securitySettings['refresh_token']);
    }

    private function updateAuthData(array $data)
    {
        // Значения которые будут обновлены в хранилище
        $updateFields = ['access_token', 'refresh_token', 'expires'];

        $this->securitySettings['access_token'] = $data['access_token'];
        $this->securitySettings['refresh_token'] = $data['refresh_token'];
        $this->securitySettings['expires'] = $data['expires'];

        if (strlen($this->securitySettings['application_redirect']) == 0) {
            $this->securitySettings['application_redirect'] = $data['application_redirect'];
        }

        foreach ($updateFields as $field) {
            $sql = "update settings set val = :val where code = :code and paramName = :paramName";
            DB::update($sql, ['code' => $this->getCode(), 'paramName' => $field, 'val' => $data[$field]]);
        }
    }


    /**
     * Установка нового приложения.
     * Сохраняем данные в БД если их еще нет.
     * Если уже есть то не перетираем
     * @param array $data Данные из реквеста
     * @param bool $isUpdate Обновлять ли параметры если уже есть
     * @return bool
     */
    public function appInstall(array $data, $isUpdate = false): bool
    {
        if ($this->isInit && (!$isUpdate)) {
            return 0;
        }

        Log::debug(print_r($data, 1));
        $security = array(
            'application_token' => $data['auth']['application_token'],
            'application_scope' => $data['auth']['scope'],
            'domain' => $data['auth']['domain'],
            'member_id' => $data['auth']['member_id'],
            'access_token' => $data['auth']['access_token'],
            'refresh_token' => $data['auth']['refresh_token'],
            'application_redirect' => '',
            'expires' => $data['auth']['expires']
        );

        foreach ($security as $index => $val) {
            $sql = "select count(1) cnt from settings s where s.code = :code and paramName = :paramName";
            $res = DB::selectOne($sql, ['code' => $this->getCode(), 'paramName' => $index]);

            if ($res->cnt == 0) {
                $sql = "insert into settings (code,paramName, val) values (:code, :paramName, :val)";
                DB::insert($sql, ['code' => $this->getCode(), 'paramName' => $index, 'val' => $val]);
            } else {
                $sql = "update settings set val = :val where code = :code and paramName = :paramName";
                DB::update($sql, ['code' => $this->getCode(), 'paramName' => $index, 'val' => $val]);
            }
        }
        return true;
    }

    /**
     * Удаление приложения
     * @return bool
     */
    public function appDelete(): bool
    {
        $sql = "delete from settings where code = :code";

        DB::delete($sql, ['code' => $this->getCode()]);

        return true;
    }

    /**
     * Формирование кода который будет использоваться в таблице настроек
     * Добавляем префикс чтобы не произошло интерференции с какими-либо другими настройками
     * @return string
     */
    private function getCode()
    {
        return "BitrixDriver_" . $this->code;
    }

    /**
     * Загрузка настроек безопасности
     */
    private function loadSecuritySettings()
    {
        $checkSettingsPresent = ['application_token', 'application_scope', 'domain', 'member_id', 'access_token', 'refresh_token', 'expires'];

        $sql = "select * from settings s where s.code = :code";
        $data = DB::select($sql, ['code' => $this->getCode()]);

        if (is_null($data)) {
            return;
        }

        $securitySettings = [];
        foreach ($data as $item) {
            $securitySettings[$item->paramName] = $item->val;
        }

        $this->securitySettings = $securitySettings;

        foreach ($checkSettingsPresent as $item) {
            if (!isset($this->securitySettings[$item])) {
                return;
            }
        }

        $this->isInit = true;
    }

    public function getAllData($method, $params = [], $limit = 0)
    {
        return $this->getInternalAllData($method, $params, $limit);
    }

    private function getInternalAllData($method, $params = [], $limit = 0, $offset = 0)
    {
        $localOffset = $offset % 50;            // Сколько записей начиная от ближайшей пачки кратной 50 записям
        $localLimit = $limit + $localOffset;  // Лимит начиная от начала пачки
        $start = $offset - $localOffset; // Позиция начала текущей пачки

        $data = [];

        $overload = false;
        $total = 0;

        do {
            //token expiration check
            $this->checkTokenExpire();

            $params['start'] = $start;
            $partData = $this->bitrix24->call($method, $params);
            if (!$partData) {
                throw new \Exception('Result is empty');
            }

            if (isset($partData['error'])) {
                return $partData;
            }

            if ($total == 0 && isset($partData['total'])) {
                $total = $partData['total'];
            }
            $data = array_merge($data, $partData['result']);

            if (($limit > 0) && count($data) >= $localLimit) {
                $overload = true;
            }

            if (isset($partData['next'])) {
                $start = $partData['next'];
            }

            sleep(1);
        } while (isset($partData['next']) && count($partData['result']) && !$overload);

        return ['result' => $data, 'total' => $total];
    }

    public function call($method, $params)
    {
        //token expiration check
        $this->checkTokenExpire();

        // execute request
        return $this->bitrix24->call($method, $params);
    }

}
